#include "IFileSystem.h"

#include "TestBase.h"

#include <gtest/gtest.h>

enum class action_existing {
  read_from_start,
  read_from_start_clear,
  read_from_start_append,
  destroy_content,
  failure,
  append,
  append_no_read,
  append_no_write,
};

enum class action_fresh {
  failure,
  error,
  create,
  create_no_write,
};

struct FileModeTestParam {
  openmode mode;
  action_existing a_existing;
  action_fresh a_fresh;
};

class FileModeTest : public ::testing::TestWithParam<
                         std::tuple<std::string, FileModeTestParam>>,
                     public TestBase {
protected:
  void SetUp() override {
    setup(std::get<0>(GetParam()));
    m_params = std::get<1>(GetParam());
  }

  void TearDown() override { teardown(); }

  FileModeTestParam m_params;
};

std::string mode_to_string(openmode mode) {
  std::string str;
  if (mode & std::ios::in) {
    str += "In";
  }
  if (mode & std::ios::out) {
    str += "Out";
  }
  if (mode & std::ios::binary) {
    str += "Bin";
  }
  if (mode & std::ios::app) {
    str += "App";
  }
  if (mode & std::ios::trunc) {
    str += "Trunc";
  }
  return str;
}

// The table in here https://en.cppreference.com/w/cpp/io/basic_filebuf/open
// is replicated in the following test cases. "noreplace" is omitted because
// it is not supported until C++23.
INSTANTIATE_TEST_SUITE_P(
    RealVsFakeOpenModes, FileModeTest,
    ::testing::Combine(
        ::testing::Values("real", "fake"),
        ::testing::Values(
            FileModeTestParam{std::ios::in, action_existing::read_from_start,
                              action_fresh::failure},
            FileModeTestParam{std::ios::binary | std::ios::in,
                              action_existing::read_from_start,
                              action_fresh::failure},
            FileModeTestParam{std::ios::in | std::ios::out,
                              action_existing::read_from_start_clear,
                              action_fresh::failure},
            FileModeTestParam{std::ios::binary | std::ios::in | std::ios::out,
                              action_existing::read_from_start_clear,
                              action_fresh::failure},
            FileModeTestParam{std::ios::out, action_existing::destroy_content,
                              action_fresh::create},
            FileModeTestParam{std::ios::out | std::ios::trunc,
                              action_existing::destroy_content,
                              action_fresh::create},
            FileModeTestParam{std::ios::binary | std::ios::out,
                              action_existing::destroy_content,
                              action_fresh::create},
            FileModeTestParam{
                std::ios::binary | std::ios::out | std::ios::trunc,
                action_existing::destroy_content, action_fresh::create},
            FileModeTestParam{std::ios::in | std::ios::out | std::ios::trunc,
                              action_existing::destroy_content,
                              action_fresh::create},
            FileModeTestParam{std::ios::binary | std::ios::in | std::ios::out |
                                  std::ios::trunc,
                              action_existing::destroy_content,
                              action_fresh::create},
            FileModeTestParam{std::ios::out | std::ios::app,
                              action_existing::append_no_read,
                              action_fresh::create},
            FileModeTestParam{std::ios::app, action_existing::append_no_write,
                              action_fresh::create_no_write},
            FileModeTestParam{std::ios::binary | std::ios::out | std::ios::app,
                              action_existing::append_no_read,
                              action_fresh::create},
            FileModeTestParam{std::ios::binary | std::ios::app,
                              action_existing::append_no_write,
                              action_fresh::create_no_write},
            FileModeTestParam{std::ios::in | std::ios::out | std::ios::app,
                              action_existing::append, action_fresh::create},
            FileModeTestParam{std::ios::in | std::ios::app,
                              action_existing::append_no_write,
                              action_fresh::create_no_write},
            FileModeTestParam{std::ios::binary | std::ios::in | std::ios::out |
                                  std::ios::app,
                              action_existing::append, action_fresh::create},
            FileModeTestParam{std::ios::binary | std::ios::in | std::ios::app,
                              action_existing::append_no_write,
                              action_fresh::create_no_write})),
    [](const ::testing::TestParamInfo<FileModeTest::ParamType> &info) {
      return std::get<0>(info.param) + "_" +
             mode_to_string(std::get<1>(info.param).mode);
    });

TEST_P(FileModeTest, ReadEmptyFile) {
  {
    auto is = when_opening(PATH_1, m_params.mode);
    switch (m_params.a_fresh) {
    case action_fresh::failure:
    case action_fresh::create:
      then_reads(is, "");
      break;
    case action_fresh::error:
      EXPECT_THROW(then_reads(is, ""), std::exception);
      break;
    }
  }
}

TEST_P(FileModeTest, ReadExistingFile) {
  {
    auto os = when_opening(PATH_1, std::ios::out);
    os << "TEXT1";
  }
  {
    auto is = when_opening(PATH_1, m_params.mode);
    switch (m_params.a_existing) {
    case action_existing::read_from_start:
    case action_existing::read_from_start_clear:
    case action_existing::read_from_start_append:
    case action_existing::append:
      then_reads(is, "TEXT1");
      break;
    case action_existing::destroy_content:
    case action_existing::failure:
    case action_existing::append_no_read:
      then_reads(is, "");
      break;
    }
  }
}

TEST_P(FileModeTest, WriteFileFresh) {
  {
    auto os = when_opening(PATH_1, m_params.mode);
    os << "TEXT1";
  }
  {
    auto is = when_opening(PATH_1);
    switch (m_params.a_fresh) {
    case action_fresh::failure:
    case action_fresh::create_no_write:
      then_reads(is, "");
      break;
    case action_fresh::error:
      EXPECT_THROW(then_reads(is, ""), std::exception);
      break;
    case action_fresh::create:
      then_reads(is, "TEXT1");
      break;
    }
  }
}

TEST_P(FileModeTest, WriteExistingFile) {
  {
    auto os = when_opening(PATH_1, std::ios::out);
    os << "PRE";
  }
  {
    auto os = when_opening(PATH_1, m_params.mode);
    os << "TEXT1";
  }
  {
    auto is = when_opening(PATH_1);
    switch (m_params.a_existing) {
    case action_existing::read_from_start:
    case action_existing::failure:
      then_reads(is, "PRE");
      break;
    case action_existing::read_from_start_clear:
    case action_existing::destroy_content:
      then_reads(is, "TEXT1");
      break;
    case action_existing::read_from_start_append:
    case action_existing::append:
      then_reads(is, "PRETEXT1");
      break;
    }
  }
}