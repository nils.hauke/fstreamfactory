#include "IFileSystem.h"

#include "TestBase.h"

#include <gtest/gtest.h>

const auto TEXT1 = "Text1";
const auto TEXT2 = "Text2";
const auto TEXT1_TEXT2 = "Text1Text2";
const auto TEXT_NON_ASCII = "ąćęłńóśźż";
const auto EMPTY = "";

class FileSystemTest : public ::testing::TestWithParam<std::string>,
                       public TestBase {
protected:
  void SetUp() override { setup(GetParam()); }
  void TearDown() override { teardown(); }
};

INSTANTIATE_TEST_SUITE_P(
    RealVsFake, FileSystemTest, ::testing::Values("real", "fake"),
    [](const testing::TestParamInfo<FileSystemTest::ParamType> &info) {
      return info.param;
    });

TEST_P(FileSystemTest, GIVEN_FileExists_WHEN_Copying_THEN_TargetContainsData) {
  {
    auto os = when_opening(PATH_1, std::ios::out);
    os << TEXT1;
  }
  when_copying();
  then_file_contains(TEXT1, PATH_2);
}

TEST_P(FileSystemTest, GIVEN_FileExists_WHEN_Copying_THEN_SourceContainsData) {
  {
    auto os = when_opening(PATH_1, std::ios::out);
    os << TEXT1;
  }
  when_copying();
  then_file_contains(TEXT1);
}

TEST_P(FileSystemTest, GIVEN_NoFile_WHEN_Copying_THEN_Error) {
  // no file created
  then_cannot_copy();
}

TEST_P(FileSystemTest,
       GIVEN_SourceAndTargetFileExists_WHEN_Copying_THEN_Error) {
  {
    auto os = when_opening(PATH_1, std::ios::out);
    os << TEXT1;
  }
  {
    auto os = when_opening(PATH_2, std::ios::out);
    os << TEXT2;
  }
  then_cannot_copy();
}

TEST_P(FileSystemTest,
       GIVEN_SourceAndTargetFileExists_WHEN_CopyingOverwrite_THEN_NoError) {
  {
    auto os = when_opening(PATH_1, std::ios::out);
    os << TEXT1;
  }
  {
    auto os = when_opening(PATH_2, std::ios::out);
    os << TEXT2;
  }
  EXPECT_NO_THROW(when_copying_with_options(copy_options::overwrite_existing));
}

TEST_P(
    FileSystemTest,
    GIVEN_AFileSpecifiedUsingAbsolutePath_WHEN_OpeningUsingRelativePath_THEN_CanRead) {
  {
    auto os = when_opening(PATH_1_ABS, std::ios::out);
    os << TEXT1;
  }
  then_file_contains(TEXT1);
}

TEST_P(
    FileSystemTest,
    GIVEN_AFileSpecifiedUsingRelativePath_WHEN_OpeningUsingAbsolutePath_THEN_CanRead) {
  {
    auto os = when_opening(PATH_1, std::ios::out);
    os << TEXT1;
  }
  then_file_contains(TEXT1, PATH_1_ABS);
}

TEST_P(FileSystemTest,
       GIVEN_AnAbsolutePath_WHEN_CalculatingAbsolutePath_THEN_ReturnsSamePath) {
  EXPECT_EQ(std::string(PATH_1_ABS), m_fs->absolute(PATH_1_ABS).string());
}

TEST_P(FileSystemTest,
       GIVEN_AnAbsolutePath_WHEN_CalculatingRelativePath_THEN_StripsCwd) {
  EXPECT_EQ(std::string(PATH_1), m_fs->relative(PATH_1_ABS).string());
}

TEST_P(FileSystemTest,
       GIVEN_ARelativePath_WHEN_CalculatingAbsolutePath_THEN_AppendsCwd) {
  EXPECT_EQ(std::string(PATH_1_ABS), m_fs->absolute(PATH_1).string());
}

TEST_P(FileSystemTest,
       GIVEN_ARelativePath_WHEN_CalculatingRelativePath_THEN_EmptyString) {
  EXPECT_EQ(std::string(""), m_fs->relative(PATH_1).string());
}

TEST_P(
    FileSystemTest,
    GIVEN_AnAbsolutePathWithBase_WHEN_CalculatingRelativePath_THEN_EmptyString) {
  EXPECT_EQ(std::string("bin/cp"),
            m_fs->relative("/usr/bin/cp", "/usr").string());
}

TEST_P(
    FileSystemTest,
    GIVEN_AnAbsolutePathWithBaseTrailingSlash_WHEN_CalculatingRelativePath_THEN_EmptyString) {
  EXPECT_EQ(std::string("bin/cp"),
            m_fs->relative("/usr/bin/cp", "/usr/").string());
}

TEST_P(
    FileSystemTest,
    GIVEN_AnAbsolutePathWithRootBase_WHEN_CalculatingRelativePath_THEN_EmptyString) {
  EXPECT_EQ(std::string("usr/bin/cp"),
            m_fs->relative("/usr/bin/cp", "/").string());
}

TEST_P(FileSystemTest,
       GIVEN_WHEN_QueryCurrentPath_THEN_ReturnsCurrentWorkingDirectory) {
  EXPECT_EQ(CWD, m_fs->current_path().string());
}

TEST_P(FileSystemTest,
       GIVEN_WHEN_ChangingCurrentPath_THEN_ReturnsCurrentWorkingDirectory) {
  m_fs->current_path("/etc");
  EXPECT_EQ("/etc", m_fs->current_path().string());
}

TEST_P(
    FileSystemTest,
    GIVEN_WHEN_ChangingCurrentPathWithTrailingSlash_THEN_ReturnsCurrentWorkingDirectory) {
  m_fs->current_path("/etc/");
  EXPECT_EQ("/etc", m_fs->current_path().string());
}

TEST_P(
    FileSystemTest,
    GIVEN_ARelativePath_WHEN_ChangingCwdAndCalculatingAbsolutePath_THEN_AppendsNewCwd) {
  m_fs->current_path("/etc");
  EXPECT_EQ(std::string("/etc/") + PATH_1, m_fs->absolute(PATH_1).string());
}

TEST_P(FileSystemTest, GIVEN_AFile_WHEN_ReadingFileSize_THEN_ReturnsFileSize) {
  {
    auto os = when_opening(PATH_1, std::ios::out);
    os << TEXT1;
  }
  EXPECT_EQ(std::string(TEXT1).size(), m_fs->file_size(PATH_1));
}

TEST_P(
    FileSystemTest,
    GIVEN_AFileWithNonAsciiCharacters_WHEN_ReadingFileSize_THEN_ReturnsFileSize) {
  {
    auto os = when_opening(PATH_1, std::ios::out);
    os << TEXT_NON_ASCII;
  }
  EXPECT_EQ(std::string(TEXT_NON_ASCII).size(), m_fs->file_size(PATH_1));
}

TEST_P(FileSystemTest,
       GIVEN_NoFile_WHEN_TryingToReadFileSize_THEN_ThrowsException) {
  EXPECT_THROW(m_fs->file_size(PATH_1), std::exception);
}

TEST_P(FileSystemTest, GIVEN_AFile_WHEN_Rename_THEN_TargetContainsData) {
  {
    auto os = when_opening(PATH_1, std::ios::out);
    os << TEXT1;
  }
  m_fs->rename(PATH_1, PATH_2);
  then_file_contains(TEXT1, PATH_2);
}

TEST_P(FileSystemTest, GIVEN_AFile_WHEN_Rename_THEN_SourceDoesNotExist) {
  {
    auto os = when_opening(PATH_1, std::ios::out);
    os << TEXT1;
  }
  m_fs->rename(PATH_1, PATH_2);
  then_file_does_not_exist(PATH_1);
}

TEST_P(FileSystemTest,
       GIVEN_TwoFiles_WHEN_TryingToRenameToExisting_THEN_Ovewrites) {
  {
    auto os = when_opening(PATH_1, std::ios::out);
    os << TEXT1;
  }
  {
    auto os = when_opening(PATH_2, std::ios::out);
    os << TEXT2;
  }
  m_fs->rename(PATH_1, PATH_2);
  then_file_contains(TEXT1, PATH_2);
}
