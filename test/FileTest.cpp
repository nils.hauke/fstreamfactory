#include "IFileSystem.h"

#include "TestBase.h"

#include <gtest/gtest.h>

using namespace std::string_literals;

const auto TEXT1 = "Text1";
const auto TEXT2 = "Text2";
const auto TEXT1_TEXT2 = "Text1Text2";
const auto TEXT_NON_ASCII = "ąćęłńóśźż";
const auto EMPTY = "";

class FileTest : public ::testing::TestWithParam<std::string>, public TestBase {
protected:
  void SetUp() override { setup(GetParam()); }
  void TearDown() override { teardown(); }
};

INSTANTIATE_TEST_SUITE_P(
    RealVsFake, FileTest, ::testing::Values("real", "fake"),
    [](const testing::TestParamInfo<FileTest::ParamType> &info) {
      return info.param;
    });

TEST_P(FileTest,
       GIVEN_AFile_WHEN_WriteInModeOutAppInBinary_THEN_AppendsContent) {
  {
    auto os = when_opening(PATH_1, std::ios::out);
    os << TEXT1;
  }
  {
    auto os = when_opening(PATH_1, std::ios::out | std::ios::app |
                                       std::ios::in | std::ios::binary);
    os << TEXT2;
  }
  then_file_contains(TEXT1_TEXT2);
}

TEST_P(FileTest, GIVEN_AFile_WHEN_WriteInModeAppInBinary_THEN_NoWrite) {
  {
    auto os = when_opening(PATH_1, std::ios::out);
    os << TEXT1;
  }
  {
    auto os =
        when_opening(PATH_1, std::ios::app | std::ios::in | std::ios::binary);
    os << TEXT2;
  }
  then_file_contains(TEXT1);
}

TEST_P(
    FileTest,
    GIVEN__AFileWithTwoLines_WHEN_ReadingInParallel_THEN_ReadersDontAffectEachOther) {
  {
    auto os = when_opening(PATH_1, std::ios::out);
    os << TEXT1 + "\n"s + TEXT2;
  }
  {
    auto is1 = when_opening(PATH_1);
    auto is2 = when_opening(PATH_1);
    then_reads(is1, TEXT1);

    then_reads(is2, TEXT1);
    then_reads(is2, TEXT2);

    then_reads(is1, TEXT2);
  }
}

TEST_P(FileTest, GIVEN_NoFile_WHEN_WritingInReadMode_THEN_FileDoesNotExist) {
  {
    auto os = when_opening(PATH_1, std::ios::in);
    os << TEXT1;
  }
  then_file_does_not_exist();
}

TEST_P(FileTest, GIVEN_NoFile_WHEN_WritingInReadMode_THEN_FileIsEmpty) {
  {
    auto os = when_opening(PATH_1, std::ios::in);
    os << TEXT1;
  }
  then_file_contains(EMPTY);
}

TEST_P(FileTest, GIVEN_NoFile_WHEN_WritingMultipleLines_THEN_ReadOneByOne) {
  {
    auto os = when_opening(PATH_1, std::ios::out);
    os << TEXT1 + "\n"s + TEXT2;
  }
  {
    auto is = when_opening(PATH_1);
    then_reads(is, TEXT1);
    then_reads(is, TEXT2);
  }
}

TEST_P(
    FileTest,
    GIVEN_NoFile_WHEN_WritingMultipleLinesWithEmptyLine_THEN_ReadOneByOneIgnoreEmptyLine) {
  {
    auto os = when_opening(PATH_1, std::ios::out);
    os << TEXT1 + "\n\n"s + TEXT2;
  }
  then_file_contains(TEXT1 + "\n\n"s + TEXT2);
}

TEST_P(FileTest, GIVEN_NoFile_WHEN_WritingMultipleWords_THEN_ReadOneByOne) {
  {
    auto os = when_opening(PATH_1, std::ios::out);
    os << TEXT1 + " "s + TEXT2;
  }
  {
    auto is = when_opening(PATH_1);
    then_reads(is, TEXT1);
    then_reads(is, TEXT2);
  }
}

TEST_P(
    FileTest,
    GIVEN_NoFile_WHEN_WritingMultipleWordsWithDoubleSpace_THEN_ReadOneByOne) {
  {
    auto os = when_opening(PATH_1, std::ios::out);
    os << TEXT1 + "  "s + TEXT2;
  }
  {
    auto is = when_opening(PATH_1);
    then_reads(is, TEXT1);
    then_reads(is, TEXT2);
  }
}

TEST_P(FileTest, GIVEN_NoFile_WHEN_WritingBinaryMode_THEN_ContainsText) {
  {
    auto os = when_opening(PATH_1, std::ios::out | std::ios::binary);
    os << TEXT1;
  }
  then_file_contains(TEXT1);
}

TEST_P(
    FileTest,
    GIVEN_NoFile_WHEN_WritingBinaryMultipleWordsWithDoubleSpace_THEN_ReadOneByOne) {
  {
    auto os = when_opening(PATH_1, std::ios::out | std::ios::binary);
    os << TEXT1 + " "s + TEXT2;
  }
  {
    auto is = when_opening(PATH_1);
    then_reads(is, TEXT1);
    then_reads(is, TEXT2);
  }
}

TEST_P(FileTest, GIVEN_NoFile_WHEN_WritingBools_THEN_FileContains101) {
  {
    auto os = when_opening(PATH_1, std::ios::out);
    os << true << false << true;
  }
  then_file_contains("101");
}

TEST_P(FileTest, GIVEN_NoFile_WHEN_WritingBool_THEN_ReadsBool) {
  {
    auto os = when_opening(PATH_1, std::ios::out);
    os << true;
  }
  {
    auto is = when_opening(PATH_1);
    bool value;
    is >> value;
    EXPECT_TRUE(value);
  }
}

TEST_P(FileTest, GIVEN_NoFile_WHEN_WritingLong_THEN_ReadsLong) {
  {
    auto os = when_opening(PATH_1, std::ios::out);
    os << 1234567890l;
  }
  {
    auto is = when_opening(PATH_1);
    long value;
    is >> value;
    EXPECT_EQ(1234567890l, value);
  }
}

TEST_P(FileTest, GIVEN_NoFile_WHEN_WritingLong_THEN_ReadsString) {
  {
    auto os = when_opening(PATH_1, std::ios::out);
    os << 1234567890l;
  }
  {
    auto is = when_opening(PATH_1);
    std::string str_read;
    is >> str_read;
    EXPECT_EQ("1234567890", str_read);
  }
}

TEST_P(FileTest, GIVEN_NoFile_WHEN_WritingUnsignedLong_THEN_ReadsULong) {
  {
    auto os = when_opening(PATH_1, std::ios::out);
    os << 1234567890ul;
  }
  {
    auto is = when_opening(PATH_1);
    unsigned long value;
    is >> value;
    EXPECT_EQ(1234567890ul, value);
  }
}

TEST_P(FileTest, GIVEN_NoFile_WHEN_WritingUnsignedLong_THEN_ReadsString) {
  {
    auto os = when_opening(PATH_1, std::ios::out);
    os << 1234567890ul;
  }
  {
    auto is = when_opening(PATH_1);
    std::string str_read;
    is >> str_read;
    EXPECT_EQ("1234567890", str_read);
  }
}

TEST_P(FileTest, GIVEN_NoFile_WHEN_WritingLongLong_THEN_ReadsLongLong) {
  {
    auto os = when_opening(PATH_1, std::ios::out);
    os << 1234567890ll;
  }
  {
    auto is = when_opening(PATH_1);
    long long value;
    is >> value;
    EXPECT_EQ(1234567890ll, value);
  }
}

TEST_P(FileTest, GIVEN_NoFile_WHEN_WritingLongLong_THEN_ReadsString) {
  {
    auto os = when_opening(PATH_1, std::ios::out);
    os << 1234567890ll;
  }
  {
    auto is = when_opening(PATH_1);
    std::string str_read;
    is >> str_read;
    EXPECT_EQ("1234567890", str_read);
  }
}

TEST_P(FileTest,
       GIVEN_NoFile_WHEN_WritingUnsignedLongLong_THEN_ReadsULongLong) {
  {
    auto os = when_opening(PATH_1, std::ios::out);
    os << 1234567890ull;
  }
  {
    auto is = when_opening(PATH_1);
    unsigned long long value;
    is >> value;
    EXPECT_EQ(1234567890ull, value);
  }
}

TEST_P(FileTest, GIVEN_NoFile_WHEN_WritingUnsignedLongLong_THEN_ReadsString) {
  {
    auto os = when_opening(PATH_1, std::ios::out);
    os << 1234567890ull;
  }
  {
    auto is = when_opening(PATH_1);
    std::string str_read;
    is >> str_read;
    EXPECT_EQ("1234567890", str_read);
  }
}

TEST_P(FileTest, GIVEN_NoFile_WHEN_WritingDouble_THEN_ReadsDouble) {
  {
    auto os = when_opening(PATH_1, std::ios::out);
    os << -123.456;
  }
  {
    auto is = when_opening(PATH_1);
    double value;
    is >> value;
    EXPECT_EQ(-123.456, value);
  }
}

TEST_P(FileTest, GIVEN_NoFile_WHEN_WritingDouble_THEN_ReadsString) {
  {
    auto os = when_opening(PATH_1, std::ios::out);
    os << -123.456;
  }
  {
    auto is = when_opening(PATH_1);
    std::string str_read;
    is >> str_read;
    EXPECT_EQ("-123.456", str_read);
  }
}

TEST_P(FileTest, GIVEN_NoFile_WHEN_WritingChar_THEN_ReadsChar) {
  {
    auto os = when_opening(PATH_1, std::ios::out);
    os << 'a' << 'b' << 'c';
  }
  {
    auto is = when_opening(PATH_1);
    std::string str_read;
    is >> str_read;
    EXPECT_EQ("abc", str_read);
  }
}

TEST_P(FileTest, GIVEN_NoFile_WHEN_WritingEndl_THEN_FileContainsTwoLines) {
  {
    auto os = when_opening(PATH_1, std::ios::out);
    os << TEXT1 << std::endl << TEXT2;
  }
  then_file_contains(TEXT1 + "\n"s + TEXT2);
}

TEST_P(FileTest, GIVEN_NoFile_WHEN_WritingTwoInts_THEN_AreConcatenated) {
  {
    auto os = when_opening(PATH_1, std::ios::out);
    os << 123 << 456;
  }
  then_file_contains("123456");
}

TEST_P(FileTest, GIVEN_AFile_WHEN_Opening_THEN_Good) {
  {
    auto os = when_opening(PATH_1, std::ios::out);
    os << TEXT1;
  }
  {
    auto is = when_opening(PATH_1);
    EXPECT_TRUE(is.good());
  }
}

TEST_P(FileTest, GIVEN_AFile_WHEN_ClearingBadBit_THEN_Bad) {
  {
    auto os = when_opening(PATH_1, std::ios::out);
    os << TEXT1;
  }
  {
    auto is = when_opening(PATH_1);
    is.clear(std::ios::badbit);
    EXPECT_TRUE(is.bad());
  }
}

TEST_P(FileTest, GIVEN_AFile_WHEN_SettingException_THEN_ReadException) {
  {
    auto os = when_opening(PATH_1, std::ios::out);
    os << TEXT1;
  }
  {
    auto is = when_opening(PATH_1);
    is.exceptions(std::ios::badbit);
    EXPECT_EQ(std::ios::badbit, is.exceptions());
  }
}

TEST_P(FileTest, GIVEN_AFileWithException_WHEN_FileGoesBad_THEN_Exception) {
  {
    auto os = when_opening(PATH_1, std::ios::out);
    os << TEXT1;
  }
  {
    auto is = when_opening(PATH_1);
    is.exceptions(std::ios::badbit);
    EXPECT_ANY_THROW(is.setstate(std::ios::badbit));
  }
}

TEST_P(FileTest,
       GIVEN_AFileWithMultipleLines_WHEN_ReadingFromRdbuf_THEN_ReadsAll) {
  {
    auto os = when_opening(PATH_1, std::ios::out);
    os << TEXT1 << std::endl << TEXT2;
  }
  {
    auto is = when_opening(PATH_1);
    std::string str_read;
    std::stringstream content;
    content << is.rdbuf();
    EXPECT_EQ(TEXT1 + "\n"s + TEXT2, content.str());
  }
}

TEST_P(FileTest,
       GIVEN_AFile_WHEN_Get_THEN_ReadsFirstCharAndIncrementsPosition) {
  {
    auto os = when_opening(PATH_1, std::ios::out);
    os << TEXT1;
  }
  {
    auto is = when_opening(PATH_1);
    char c;
    is.get(c);
    EXPECT_EQ(TEXT1[0], c);
    is.get(c);
    EXPECT_EQ(TEXT1[1], c);
  }
}

TEST_P(FileTest, GIVEN_AFile_WHEN_GetSecondTime_THEN_ReadsSecondCharacter) {
  {
    auto os = when_opening(PATH_1, std::ios::out);
    os << TEXT1;
  }
  {
    auto is = when_opening(PATH_1);
    char c;
    is.get(c);
    is.get(c);
    EXPECT_EQ(TEXT1[1], c);
  }
}

TEST_P(FileTest, GIVEN_AFile_WHEN_Get_THEN_ReadsFirstCharacter) {
  {
    auto os = when_opening(PATH_1, std::ios::out);
    os << TEXT1;
  }
  {
    auto is = when_opening(PATH_1);
    EXPECT_EQ(TEXT1[0], is.get());
  }
}

TEST_P(FileTest, GIVEN_AFile_WHEN_Peak_THEN_ReadsFirstCharacter) {
  {
    auto os = when_opening(PATH_1, std::ios::out);
    os << TEXT1;
  }
  {
    auto is = when_opening(PATH_1);
    EXPECT_EQ(TEXT1[0], is.peek());
  }
}

TEST_P(FileTest, GIVEN_AFile_WHEN_Peak_THEN_DoesntAdvancePosition) {
  {
    auto os = when_opening(PATH_1, std::ios::out);
    os << TEXT1;
  }
  {
    auto is = when_opening(PATH_1);
    is.peek();
    EXPECT_EQ(TEXT1[0], is.get());
  }
}

TEST_P(FileTest, GIVEN_AFile_WHEN_Unget_THEN_GetsPreviousCharacter) {
  {
    auto os = when_opening(PATH_1, std::ios::out);
    os << TEXT1;
  }
  {
    auto is = when_opening(PATH_1);
    is.get();
    is.unget();
    EXPECT_EQ(TEXT1[0], is.get());
  }
}

TEST_P(FileTest, GIVEN_AFile_WHEN_Putback_THEN_GetsPreviousCharacter) {
  {
    auto os = when_opening(PATH_1, std::ios::out);
    os << TEXT1;
  }
  {
    auto is = when_opening(PATH_1);
    is.get();
    is.putback('?');
    EXPECT_EQ('?', is.get());
  }
}

TEST_P(FileTest, GIVEN_AFileWithMultipleLines_WHEN_Getline_THEN_ReadsLine) {
  {
    auto os = when_opening(PATH_1, std::ios::out);
    os << TEXT1 << std::endl << TEXT2;
  }
  {
    auto is = when_opening(PATH_1);
    char buffer[100];
    is.getline(buffer, std::string(TEXT1).size() + 4);
    EXPECT_EQ(TEXT1, std::string(buffer));
  }
}

TEST_P(
    FileTest,
    GIVEN_AFileWithMultipleLines_WHEN_GetlineWithOtherDelim_THEN_ReadsUntilDelim) {
  {
    auto os = when_opening(PATH_1, std::ios::out);
    os << "ABC.DEF" << std::endl << "ABC,DEF";
  }
  {
    auto is = when_opening(PATH_1);
    char buffer[100];
    is.getline(buffer, std::string(TEXT1).size() + 9, ',');
    EXPECT_EQ("ABC.DEF\nABC", std::string(buffer));
  }
}

TEST_P(
    FileTest,
    GIVEN_AFileWithMultipleLines_WHEN_Ignore4Characters_THEN_IgnoresCharacters) {
  {
    auto os = when_opening(PATH_1, std::ios::out);
    os << "ABC.DEF" << std::endl << "ABC,DEF";
  }
  {
    auto is = when_opening(PATH_1);
    is.ignore(4);
    char buffer[100];
    is.getline(buffer, std::string(TEXT1).size() + 9, ',');
    EXPECT_EQ("DEF\nABC", std::string(buffer));
  }
}

TEST_P(
    FileTest,
    GIVEN_AFileWithMultipleLines_WHEN_IgnoreUntilComma_THEN_IgnoresCharacters) {
  {
    auto os = when_opening(PATH_1, std::ios::out);
    os << "ABC.DEF" << std::endl << "ABC,DEF";
  }
  {
    auto is = when_opening(PATH_1);
    is.ignore(std::numeric_limits<std::streamsize>::max(), ',');
    std::string str;
    is >> str;
    EXPECT_EQ("DEF", str);
  }
}

TEST_P(
    FileTest,
    GIVEN_AFileWithMultipleLines_WHEN_Read4Characters_THEN_IgnoresCharacters) {
  {
    auto os = when_opening(PATH_1, std::ios::out);
    os << "ABC.DEF" << std::endl << "ABC,DEF";
  }
  {
    auto is = when_opening(PATH_1);
    char buffer[100];
    is.read(buffer, 4);
    EXPECT_EQ("ABC.", std::string(buffer));
  }
}

TEST_P(
    FileTest,
    GIVEN_AFileWithMultipleLines_WHEN_ReadUntilComma_THEN_IgnoresCharacters) {
  {
    auto os = when_opening(PATH_1, std::ios::out);
    os << "ABC.DEF" << std::endl << "ABC,DEF";
  }
  {
    auto is = when_opening(PATH_1);
    char buffer[100];
    is.getline(buffer, 100, ',');
    EXPECT_EQ("ABC.DEF\nABC", std::string(buffer));
  }
}

TEST_P(FileTest,
       GIVEN_AFileWithMultipleLines_WHEN_Read4Characters_THEN_GCountIs4) {
  {
    auto os = when_opening(PATH_1, std::ios::out);
    os << "ABC.DEF" << std::endl << "ABC,DEF";
  }
  {
    auto is = when_opening(PATH_1);
    char buffer[100];
    is.read(buffer, 4);
    EXPECT_EQ(4, is.gcount());
  }
}
TEST_P(FileTest,
       GIVEN_AFileWithMultipleLines_WHEN_ReadFirstLine_THEN_TellgIs7) {
  {
    auto os = when_opening(PATH_1, std::ios::out);
    os << "ABC.DEF" << std::endl << "ABC,DEF";
  }
  {
    auto is = when_opening(PATH_1);
    std::string str;
    is >> str;
    EXPECT_EQ(7, is.tellg());
  }
}

TEST_P(FileTest, GIVEN_AFileFullyRead_WHEN_SeekGToMiddle_THEN_ReadsFromMiddle) {
  {
    auto os = when_opening(PATH_1, std::ios::out);
    os << "ABCD";
  }
  {
    auto is = when_opening(PATH_1);
    std::string str;
    is >> str;
    is.seekg(2);
    is >> str;
    EXPECT_EQ("CD", str);
  }
}

TEST_P(FileTest, GIVEN_NoFile_WHEN_PutChars_THEN_FileContainsChars) {
  {
    auto os = when_opening(PATH_1, std::ios::out);
    os.put('a').put('b').put('c');
  }
  then_file_contains("abc");
}

TEST_P(FileTest, GIVEN_NoFile_WHEN_WriteChars_THEN_FileContainsChars) {
  {
    auto os = when_opening(PATH_1, std::ios::out);
    os.write("abc", 3);
  }
  then_file_contains("abc");
}

TEST_P(FileTest, GIVEN_OpenFileWithWrittenContent_WHEN_Tellp_THEN_Position) {
  auto os = when_opening(PATH_1, std::ios::out);
  os << "ABCD";
  EXPECT_EQ(4, os.tellp());
}

TEST_P(FileTest,
       GIVEN_OpenFileWithWrittenContent_WHEN_Seekp_THEN_PositionIsSet) {
  {
    auto os = when_opening(PATH_1, std::ios::out);
    os << "ABCD";
    os.seekp(2);
    os << "EF";
  }
  then_file_contains("ABEF");
}

TEST_P(FileTest, GIVEN_AFile_WHEN_SetLocale_THEN_ReadLocale) {
  auto os = when_opening(PATH_1, std::ios::out);
  os.imbue(std::locale("pl_PL.UTF-8"));
  EXPECT_EQ("pl_PL.UTF-8", os.getloc().name());
}

TEST_P(FileTest, GIVEN_AFile_THEN_DefaultLocaleIsC) {
  auto os = when_opening(PATH_1, std::ios::out);
  EXPECT_EQ("C", os.getloc().name());
}

TEST_P(
    FileTest,
    GIVEN_AFileStream_WHEN_SetHexFlagInStream_AND_WriteNumber_THEN_FileContainsHexNumber) {
  {
    auto os = when_opening(PATH_1, std::ios::out);
    os << std::hex << 255;
  }
  then_file_contains("ff");
}

TEST_P(
    FileTest,
    GIVEN_AFileStream_WHEN_SetHexFlagWithFunction_AND_WriteNumber_THEN_FileContainsHexNumber) {
  {
    auto os = when_opening(PATH_1, std::ios::out);
    os.flags(std::ios::hex);
    os << 255;
  }
  then_file_contains("ff");
}

TEST_P(FileTest,
       GIVEN_AFileStream_WHEN_SetHexFlagWithFunction_THEN_ReadsHexFlag) {
  auto os = when_opening(PATH_1, std::ios::out);
  os.flags(std::ios::hex);
  EXPECT_EQ(std::ios::hex, os.flags());
}

TEST_P(FileTest, GIVEN_AFileStream_THEN_ReadsStandardFlags) {
  auto os = when_opening(PATH_1, std::ios::out);
  EXPECT_EQ(std::ios::skipws | std::ios::dec, os.flags());
}

TEST_P(
    FileTest,
    GIVEN_NoFile_WHEN_SetPrecision_AND_WriteFloat_THEN_FileContainsFloatWithPrecision) {
  {
    auto os = when_opening(PATH_1, std::ios::out);
    os.precision(2);
    os << 123.4;
  }
  then_file_contains("1.2e+02");
}

TEST_P(FileTest, GIVEN_AFileStream_WHEN_SetPrecision_THEN_ReadsPrecision) {
  auto os = when_opening(PATH_1, std::ios::out);
  os.precision(4);
  EXPECT_EQ(4, os.precision());
}

TEST_P(
    FileTest,
    GIVEN_NoFile_WHEN_SetWidth_AND_WriteString_THEN_FileContainsStringWithWidth) {
  {
    auto os = when_opening(PATH_1, std::ios::out);
    os.width(10);
    os << "abc";
  }
  then_file_contains("       abc");
}

TEST_P(FileTest, GIVEN_AFileStream_WHEN_SetWidth_THEN_ReadsWidth) {
  auto os = when_opening(PATH_1, std::ios::out);
  os.width(10);
  EXPECT_EQ(10, os.width());
}

void callback(std::ios_base::event evt, std::ios_base &str, int idx) {
  if (evt == std::ios_base::imbue_event) {
    str.precision(3);
  }
}

TEST_P(FileTest,
       GIVEN_AFileStream_WHEN_RegisterCallback_THEN_CallbackIsCalled) {
  auto os = when_opening(PATH_1, std::ios::out);

  os.register_callback(callback, 0);
  os.imbue(std::locale("pl_PL.UTF-8"));
  EXPECT_EQ(3, os.precision());
}
