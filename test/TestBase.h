#pragma once

#include "FakeFileSystem.h"
#include "FileSystem.h"
#include "IFileSystem.h"

#include <boost/filesystem.hpp>
#include <gtest/gtest.h>

constexpr auto CWD = "/tmp";
constexpr auto PATH_1_ABS = "/tmp/test1.txt";
constexpr auto PATH_1 = "test1.txt";
constexpr auto PATH_2 = "test2.txt";

using namespace wbx::file_system;

struct TestBase {
  void setup(std::string const &kind) {
    if (kind == "real") {
      boost::filesystem::current_path(CWD);
      m_fs = std::make_shared<FileSystem>();
    } else {
      m_fs = std::make_shared<FakeFileSystem>(CWD);
    }
  }

  void teardown() {
    boost::filesystem::remove(PATH_1);
    boost::filesystem::remove(PATH_2);
  }

  IOStream when_opening(path const &path, openmode const &mode = std::ios::in) {
    return m_fs->open(path, mode);
  }

  void when_copying() { when_copying_with_options(copy_options::none); }

  void when_copying_with_options(copy_options const &options) {
    m_fs->copy(PATH_1, PATH_2, options);
  }

  void then_reads(IOStream &is, std::string const &str_expected) {
    std::string str_read;
    is >> str_read;
    EXPECT_EQ(str_expected, str_read);
  }

  void then_file_contains(std::string const &str_expected,
                          path const &path = PATH_1) {
    IOStream is = when_opening(path);
    std::stringstream str_read;
    str_read << is.rdbuf();
    EXPECT_EQ(str_expected, str_read.str());
  }

  void then_file_exists(path const &path = PATH_1) {
    EXPECT_TRUE(m_fs->exists(path));
  }

  void then_file_does_not_exist(path const &path = PATH_1) {
    EXPECT_FALSE(m_fs->exists(path));
  }

  void then_cannot_copy() { EXPECT_THROW(when_copying(), std::exception); }

  std::shared_ptr<IFileSystem> m_fs;
};