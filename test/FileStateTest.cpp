#include "IFileSystem.h"

#include "TestBase.h"

#include <gtest/gtest.h>

struct FileStateTestParam {
  bool good_bit;
  bool eof_bit;
  bool fail_bit;
  bool bad_bit;

  bool is_good;
  bool is_fail;
  bool is_bad;
  bool is_eof;
  bool is_op_bool;
  bool is_op_not;
};

class FileStateTest : public ::testing::TestWithParam<
                          std::tuple<std::string, FileStateTestParam>>,
                      public TestBase {
protected:
  void SetUp() override {
    setup(std::get<0>(GetParam()));
    m_params = std::get<1>(GetParam());
  }

  void TearDown() override { teardown(); }

  void when_set_state(IOStream &is) {
    is.setstate(std::ios_base::iostate(
        (m_params.good_bit ? std::ios_base::goodbit : 0) |
        (m_params.eof_bit ? std::ios_base::eofbit : 0) |
        (m_params.fail_bit ? std::ios_base::failbit : 0) |
        (m_params.bad_bit ? std::ios_base::badbit : 0)));
  }

  FileStateTestParam m_params;
};

// This is testing the table at the bottom of
// https://en.cppreference.com/w/cpp/io/basic_ios/good.
// clang-format off
INSTANTIATE_TEST_SUITE_P(
    RealVsFakeStates, FileStateTest,
    ::testing::Combine(
        ::testing::Values("real", "fake"),
        ::testing::Values(
            //                 good     eof   fail    bad  is_good is_fail is_bad is_eof is_op_bool is_op_not
            FileStateTestParam{ true, false, false, false,    true,  false, false, false,      true,    false},
            FileStateTestParam{false, false, false,  true,   false,   true,  true, false,     false,     true},
            FileStateTestParam{false, false,  true, false,   false,   true, false, false,     false,     true},
            FileStateTestParam{false, false,  true,  true,   false,   true,  true, false,     false,     true},
            FileStateTestParam{false,  true, false, false,   false,  false, false,  true,      true,    false},
            FileStateTestParam{false,  true, false,  true,   false,   true,  true,  true,     false,     true},
            FileStateTestParam{false,  true,  true, false,   false,   true, false,  true,     false,     true},
            FileStateTestParam{false,  true,  true,  true,   false,   true,  true,  true,     false,     true}
            )),
    [](const ::testing::TestParamInfo<FileStateTest::ParamType> &info) {
      return (std::get<0>(info.param) + "_" +
              (std::get<1>(info.param).good_bit ? "good" : "not_good") + "_" +
              (std::get<1>(info.param).eof_bit ? "eof" : "not_eof") + "_" +
              (std::get<1>(info.param).fail_bit ? "fail" : "not_fail") + "_" +
              (std::get<1>(info.param).bad_bit ? "bad" : "not_bad"));
    });
// clang-format on

TEST_P(FileStateTest, Good) {
  {
    auto os = when_opening(PATH_1, std::ios::out);
    os << "TEXT1";
  }
  {
    auto is = when_opening(PATH_1);
    when_set_state(is);
    EXPECT_EQ(m_params.is_good, is.good());
  }
}

TEST_P(FileStateTest, Fail) {
  {
    auto os = when_opening(PATH_1, std::ios::out);
    os << "TEXT1";
  }
  {
    auto is = when_opening(PATH_1);
    when_set_state(is);
    EXPECT_EQ(m_params.is_fail, is.fail());
  }
}

TEST_P(FileStateTest, Bad) {
  {
    auto os = when_opening(PATH_1, std::ios::out);
    os << "TEXT1";
  }
  {
    auto is = when_opening(PATH_1);
    when_set_state(is);
    EXPECT_EQ(m_params.is_bad, is.bad());
  }
}

TEST_P(FileStateTest, Eof) {
  {
    auto os = when_opening(PATH_1, std::ios::out);
    os << "TEXT1";
  }
  {
    auto is = when_opening(PATH_1);
    when_set_state(is);
    EXPECT_EQ(m_params.is_eof, is.eof());
  }
}

TEST_P(FileStateTest, OpBool) {
  {
    auto os = when_opening(PATH_1, std::ios::out);
    os << "TEXT1";
  }
  {
    auto is = when_opening(PATH_1);
    when_set_state(is);
    EXPECT_EQ(m_params.is_op_bool, static_cast<bool>(is));
  }
}

TEST_P(FileStateTest, OpNot) {
  {
    auto os = when_opening(PATH_1, std::ios::out);
    os << "TEXT1";
  }
  {
    auto is = when_opening(PATH_1);
    when_set_state(is);
    EXPECT_EQ(m_params.is_op_not, !is);
  }
}
