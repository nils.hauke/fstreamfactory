#pragma once

#include "IFileSystem.h"
#include "IOStream.h"

#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>

#include <fstream>

namespace wbx {
namespace file_system {

class FileSystem : public IFileSystem {
public:
  IOStream open(const path &path, openmode mode = std::ios::in) override;

  void copy(const path &from, const path &to,
            copy_options options = copy_options::none) override;

  bool exists(const path &path) override;

  path absolute(const path &p) const override;
  path relative(const path &p) const override;
  path relative(const path &p, const path &base) const override;
  path current_path() const override;
  void current_path(const path &p) override;

  std::uintmax_t file_size(const path &p) const override;

  void rename(const path &from, const path &to) override;
};

} // namespace file_system
} // namespace wbx
