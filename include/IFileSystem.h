#pragma once

#include "IOStream.h"

#include <boost/filesystem.hpp>

#include <fstream>
#include <functional>
#include <ios>
#include <iostream>
#include <istream>
#include <map>
#include <memory>
#include <ostream>
#include <sstream>
#include <string>

namespace wbx {
namespace file_system {

using path = boost::filesystem::path;
using copy_options = boost::filesystem::copy_options;

class IFileSystem {
public:
  virtual ~IFileSystem() {}

  virtual IOStream open(const path &path, openmode mode = std::ios::in) = 0;

  virtual void copy(const path &from, const path &to,
                    copy_options options = copy_options::none) = 0;
  virtual void copy_file(const path &from, const path &to,
                         copy_options options = copy_options::none) {
    copy(from, to, options);
  }

  virtual bool exists(const path &path) = 0;

  virtual path absolute(const path &p) const = 0;
  virtual path relative(const path &p) const = 0;
  virtual path relative(const path &p, const path &base) const = 0;
  virtual path current_path() const = 0;
  virtual void current_path(const path &p) = 0;

  virtual std::uintmax_t file_size(const path &p) const = 0;

  virtual void rename(const path &from, const path &to) = 0;
};

} // namespace file_system
} // namespace wbx
