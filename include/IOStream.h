#pragma once

#include <iostream>
#include <memory>

#include <functional>

namespace wbx {
namespace file_system {

using openmode = std::ios_base::openmode;

class IOStream {
public:
  using CharT = char;
  using Traits = std::char_traits<CharT>;
  using iostream = std::basic_iostream<CharT>;
  using istream = std::basic_istream<CharT>;
  using ostream = std::basic_ostream<CharT>;
  using ios = std::basic_ios<CharT>;
  using streambuf = std::basic_streambuf<CharT>;

  using int_type = iostream::int_type;
  using char_type = std::istream::char_type;
  using off_type = std::streamoff;
  using pos_type = std::streampos;
  using fmtflags = std::ios_base::fmtflags;
  using event_callback = std::ios_base::event_callback;

  IOStream(std::shared_ptr<iostream> p_stream, openmode const &p_mode,
           std::function<void()> p_first_write_clean = nullptr)
      : m_mode(p_mode), m_stream(std::move(p_stream)),
        m_first_write_clean(p_first_write_clean) {}

  virtual ~IOStream() {}

  iostream &getStream() { return *m_stream; }

  IOStream &operator<<(long value) { return write(value); }
  IOStream &operator<<(unsigned long value) { return write(value); }
  IOStream &operator<<(long long value) { return write(value); }
  IOStream &operator<<(unsigned long long value) { return write(value); }
  IOStream &operator<<(double value) { return write(value); }
  IOStream &operator<<(long double value) { return write(value); }

  IOStream &operator<<(bool value) { return write(value); }
  IOStream &operator<<(void *value) { return write(value); }

  IOStream &operator<<(short value) { return write(value); }
  IOStream &operator<<(int value) { return write(value); }
  IOStream &operator<<(unsigned short value) { return write(value); }
  IOStream &operator<<(unsigned int value) { return write(value); }
  IOStream &operator<<(float value) { return write(value); }

  IOStream &operator<<(std::string value) { return write(value); }
  IOStream &operator<<(char value) { return write(value); }
  IOStream &operator<<(char const *value) { return write(value); }

  IOStream &operator<<(streambuf *value) { return write(value); }
  IOStream &operator<<(std::ios_base &(*value)(std::ios_base &)) {
    return write(value);
  }
  IOStream &operator<<(ios &(*value)(ios &)) { return write(value); }
  IOStream &operator<<(ostream &(*value)(ostream &)) { return write(value); }

  IOStream &operator>>(long &value) { return read(value); }
  IOStream &operator>>(unsigned long &value) { return read(value); }
  IOStream &operator>>(long long &value) { return read(value); }
  IOStream &operator>>(unsigned long long &value) { return read(value); }
  IOStream &operator>>(double &value) { return read(value); }
  IOStream &operator>>(long double &value) { return read(value); }

  IOStream &operator>>(bool &value) { return read(value); }
  IOStream &operator>>(void *&value) { return read(value); }

  IOStream &operator>>(short &value) { return read(value); }
  IOStream &operator>>(int &value) { return read(value); }
  IOStream &operator>>(unsigned short &value) { return read(value); }
  IOStream &operator>>(unsigned int &value) { return read(value); }
  IOStream &operator>>(float &value) { return read(value); }

  IOStream &operator>>(std::string &str) { return read(str); }
  IOStream &operator>>(char &value) { return read(value); }
  IOStream &operator>>(char *value) { return read(value); }

  IOStream &operator>>(streambuf *value) { return read(value); }
  IOStream &operator>>(std::ios_base &(*value)(std::ios_base &)) {
    return read(value);
  }
  IOStream &operator>>(ios &(*value)(ios &)) { return read(value); }
  IOStream &operator>>(istream &(*value)(istream &)) { return read(value); }

  int_type get() { return m_stream->get(); }
  istream &get(char_type &c) { return m_stream->get(c); }
  istream &get(char_type *s, std::streamsize count) {
    return m_stream->get(s, count);
  }
  istream &get(char_type *s, std::streamsize count, char_type delim) {
    return m_stream->get(s, count, delim);
  }
  istream &get(streambuf &strbuf) { return m_stream->get(strbuf); }
  istream &get(streambuf &strbuf, char_type delim) {
    return m_stream->get(strbuf, delim);
  }
  int_type peek() { return m_stream->peek(); }
  istream &unget() { return m_stream->unget(); }
  istream &putback(char_type c) { return m_stream->putback(c); }
  istream &getline(char_type *s, std::streamsize count) {
    return m_stream->getline(s, count);
  }
  istream &getline(char_type *s, std::streamsize count, char_type delim) {
    return m_stream->getline(s, count, delim);
  }
  istream &ignore(std::streamsize count = 1, int_type delim = Traits::eof()) {
    return m_stream->ignore(count, delim);
  }
  istream &read(char_type *s, std::streamsize count) {
    return m_stream->read(s, count);
  }
  std::streamsize readsome(char_type *s, std::streamsize count) {
    return m_stream->readsome(s, count);
  }
  std::streamsize gcount() const { return m_stream->gcount(); }
  pos_type tellg() { return m_stream->tellg(); }
  istream &seekg(pos_type pos) { return m_stream->seekg(pos); }
  int sync() { return m_stream->sync(); }

  ostream &put(char_type c) { return m_stream->put(c); }
  ostream &write(const char_type *s, std::streamsize count) {
    return m_stream->write(s, count);
  }
  pos_type tellp() { return m_stream->tellp(); }
  ostream &seekp(pos_type pos) { return m_stream->seekp(pos); }
  ostream &seekp(off_type off, std::ios_base::seekdir dir) {
    return m_stream->seekp(off, dir);
  }
  ostream &flush() { return m_stream->flush(); }

  bool good() const { return m_stream->good(); }
  bool eof() const { return m_stream->eof(); }
  bool fail() const { return m_stream->fail(); }
  bool bad() const { return m_stream->bad(); }
  bool operator!() const { return !static_cast<bool>(*m_stream); }
  operator bool() const { return static_cast<bool>(*m_stream); }
  std::ios::iostate rdstate() const { return m_stream->rdstate(); }
  void setstate(std::ios::iostate state) { m_stream->setstate(state); }
  void clear(std::ios::iostate state = std::ios::goodbit) {
    m_stream->clear(state);
  }
  std::ios::iostate exceptions() const { return m_stream->exceptions(); }
  void exceptions(std::ios::iostate state) { m_stream->exceptions(state); }
  std::locale imbue(const std::locale &loc) { return m_stream->imbue(loc); }
  streambuf *rdbuf() { return m_stream->rdbuf(); }
  ostream *tie() const { return m_stream->tie(); }
  ostream *tie(ostream *ostr) { return m_stream->tie(ostr); }
  char narrow(char_type c, char dfault) const {
    return m_stream->narrow(c, dfault);
  }
  char_type widen(char c) const { return m_stream->widen(c); }

  fmtflags flags() const { return m_stream->flags(); }
  fmtflags flags(fmtflags fmtfl) { return m_stream->flags(fmtfl); }
  fmtflags setf(fmtflags fmtfl) { return m_stream->setf(fmtfl); }
  fmtflags setf(fmtflags fmtfl, fmtflags mask) {
    return m_stream->setf(fmtfl, mask);
  }
  void unsetf(fmtflags mask) { m_stream->unsetf(mask); }
  std::streamsize precision() const { return m_stream->precision(); }
  std::streamsize precision(std::streamsize prec) {
    return m_stream->precision(prec);
  }
  std::streamsize width() const { return m_stream->width(); }
  std::streamsize width(std::streamsize wide) { return m_stream->width(wide); }
  std::locale getloc() const { return m_stream->getloc(); }
  long &iword(int index) { return m_stream->iword(index); }
  void *&pword(int index) const { return m_stream->pword(index); }
  void register_callback(event_callback fn, int index) {
    m_stream->register_callback(fn, index);
  }

private:
  template <typename T> inline IOStream &write(T const &value) {
    if (m_mode & std::ios::out) {
      if (m_first_write_clean != nullptr) {
        m_first_write_clean();
        m_first_write_clean = nullptr;
      }
      *m_stream << value;
    }
    return *this;
  }

  template <typename T> inline IOStream &read(T &value) {
    if (m_mode & std::ios::in) {
      *m_stream >> value;
    }
    return *this;
  }

  const openmode m_mode;
  std::shared_ptr<iostream> const m_stream;
  std::function<void()> m_first_write_clean;
};

} // namespace file_system
} // namespace wbx
