#pragma once

#include "IFileSystem.h"

#include <map>
#include <sstream>

namespace wbx {
namespace file_system {

class FakeFileSystem : public IFileSystem {
public:
  class Exception : public std::exception {
  public:
    Exception(std::string const &msg) : m_msg(msg) {}

    virtual const char *what() const noexcept override { return m_msg.c_str(); }

  private:
    std::string const m_msg;
  };

  FakeFileSystem(path p_current_working_directory);
  virtual ~FakeFileSystem() {}
  IOStream open(const path &path, openmode mode = std::ios::in) override;

  void copy(const path &from, const path &to,
            copy_options options = copy_options::none) override;

  bool exists(const path &path) override;

  path absolute(const path &p) const override;
  path relative(const path &p) const override;
  path relative(const path &p, const path &base) const override;
  path current_path() const override;
  void current_path(const path &p) override;

  std::uintmax_t file_size(const path &p) const override;

  void rename(const path &from, const path &to) override;

private:
  path m_current_working_directory;

  std::map<std::string, std::shared_ptr<std::stringstream>> m_files;
};

} // namespace file_system
} // namespace wbx
