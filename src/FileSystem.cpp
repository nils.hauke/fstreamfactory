#include "FileSystem.h"

namespace wbx {
namespace file_system {

IOStream FileSystem::open(const path &path, openmode mode) {
  return IOStream(std::make_shared<std::fstream>(path.string(), mode), mode);
}

void FileSystem::copy(const path &from, const path &to, copy_options options) {
  boost::filesystem::copy_file(from, to, options);
}

bool FileSystem::exists(const path &path) {
  return boost::filesystem::exists(path);
}

path FileSystem::absolute(const path &p) const {
  return boost::filesystem::absolute(p);
}
path FileSystem::relative(const path &p) const {
  return boost::filesystem::relative(p);
}
path FileSystem::relative(const path &p, const path &base) const {
  return boost::filesystem::relative(p, base);
}
path FileSystem::current_path() const {
  return boost::filesystem::current_path();
}
void FileSystem::current_path(const path &p) {
  boost::filesystem::current_path(p);
}

std::uintmax_t FileSystem::file_size(const path &p) const {
  return boost::filesystem::file_size(p);
}

void FileSystem::rename(const path &from, const path &to) {
  boost::filesystem::rename(from, to);
}

} // namespace file_system
} // namespace wbx
