#include "FakeFileSystem.h"

namespace wbx {
namespace file_system {

FakeFileSystem::FakeFileSystem(path p_current_working_directory) {
  current_path(p_current_working_directory);
}
IOStream FakeFileSystem::open(const path &path, openmode mode) {
  bool has_in = (mode & std::ios::in) != 0;
  bool has_out = (mode & std::ios::out) != 0;
  bool has_app = (mode & std::ios::app) != 0;
  bool has_trunc = (mode & std::ios::trunc) != 0;
  bool file_exists = m_files.find(absolute(path).string()) != m_files.end();

  if (!has_out && !file_exists) {
    // No actual file created, return dummy.
    return IOStream(std::static_pointer_cast<std::iostream>(
                        std::make_shared<std::stringstream>()),
                    mode);
  }

  if ((has_out && !has_in && !has_app) || has_trunc) {
    // Truncate file if opened in write mode.
    m_files[absolute(path).string()] = std::make_shared<std::stringstream>();
  }

  if (!has_app && !has_trunc && !file_exists && (has_in && has_out)) {
    // Writing not allowed in this configuration.
    mode = mode & ~std::ios::out;
    has_out = false;
  }

  if (!file_exists) {
    // Create file if it doesn't exist.
    m_files[absolute(path).string()] = std::make_shared<std::stringstream>();
  }

  std::shared_ptr<std::stringstream> stream;
  if (has_out) {
    stream = m_files[absolute(path).string()];
  } else {
    stream = std::make_shared<std::stringstream>(
        m_files[absolute(path).string()]->str());
  }

  std::function<void()> first_write_clear;
  if (has_in && has_out && !has_app) {
    first_write_clear = [stream]() {
      std::stringstream ss;
      stream->swap(ss);
    };
  }

  return IOStream(stream, mode, first_write_clear);
}

void FakeFileSystem::copy(const path &from, const path &to,
                          copy_options options) {
  if (options != copy_options::none &&
      options != copy_options::overwrite_existing) {
    throw Exception(
        "FakeFileSystem::copy does not support the provided options.");
  }

  if (m_files.find(absolute(from).string()) == m_files.end()) {
    throw Exception("File '" + from.string() + "' does not exist.");
  }

  if (m_files.find(absolute(to).string()) != m_files.end() &&
      (static_cast<unsigned int>(options) &
       static_cast<unsigned int>(copy_options::overwrite_existing)) == 0) {
    throw Exception("File '" + to.string() + "' already exists.");
  }

  m_files[absolute(to).string()] = std::make_shared<std::stringstream>(
      m_files[absolute(from).string()]->str());
}

bool FakeFileSystem::exists(const path &path) {
  return m_files.find(absolute(path).string()) != m_files.end();
}

path FakeFileSystem::absolute(const path &p) const {
  if (p.is_absolute()) {
    return p;
  } else {
    return m_current_working_directory / p;
  }
}

path FakeFileSystem::relative(const path &p) const {
  return relative(p, m_current_working_directory);
}

path FakeFileSystem::relative(const path &p, const path &base) const {
  if (p.is_absolute()) {
    auto baseStr = base.string();
    if (baseStr.back() != '/') {
      baseStr += '/';
    }
    return p.string().substr(baseStr.size());
  } else {
    return "";
  }
}

path FakeFileSystem::current_path() const {
  return m_current_working_directory;
}

void FakeFileSystem::current_path(const path &p) {
  if (p.string()[p.size() - 1] != '/') {
    m_current_working_directory = p;
  } else {
    m_current_working_directory = p.string().substr(0, p.size() - 1);
  }
}

std::uintmax_t FakeFileSystem::file_size(const path &p) const {
  if (m_files.find(absolute(p).string()) == m_files.end()) {
    throw Exception("File '" + p.string() + "' does not exist.");
  }

  return m_files.at(absolute(p).string())->str().size();
}

void FakeFileSystem::rename(const path &from, const path &to) {
  if (m_files.find(absolute(from).string()) == m_files.end()) {
    throw Exception("File '" + from.string() + "' does not exist.");
  }

  m_files[absolute(to).string()] = m_files[absolute(from).string()];
  m_files.erase(absolute(from).string());
}

} // namespace file_system
} // namespace wbx
